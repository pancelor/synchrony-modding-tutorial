## unofficial docs!

welcome to pancelor's unofficial temporary (:fingers_crossed:)
synchrony modding documentation/tutorial

this will teach you enough to get started writing mods for synchrony

## outline

setup
* [discord](#discord)
* [install synchrony](#install-synchrony)
* [general modding tips](#general-modding-tips)
* [run an example mod](#run-an-example-mod)
* [synchrony architecture overview](#synchrony-architecture-overview)
* [components](#components)
* [lua gotchas](#lua-gotchas)

making a mod
* [mod-making: required tools](#mod-making-required-tools)
* [example mods](#example-mods)
* [making a basic mod](#making-a-basic-mod)
* [file paths](#file-paths)
* [how do I add my custom enemy to the game?](#how-do-i-add-my-custom-enemy-to-the-game)
* [how do I log stuff to the console?](#how-do-i-log-stuff-to-the-console)
* [how do I spawn in my custom stuff, for testing?](#how-do-i-spawn-in-my-custom-stuff-for-testing)
* [how do I refer to existing stuff?](#how-do-i-refer-to-existing-stuff)
* [namespacing / how do I refer to modded stuff?](#namespacing-how-do-i-refer-to-modded-stuff)
* [what components exist?](#what-components-exist)
* [changing components on an entity](#changing-components-on-an-entity)
* [how do I add a custom spell?](#how-do-i-add-a-custom-spell)
* [different event kinds](#different-event-kinds)
* [mod dependencies](#mod-dependencies)
* [custom weapons](#custom-weapons)
* [custom characters](#custom-characters)
* [mod settings](#mod-settings)

other
* [publishing](#publishing)
* [gitlab](#gitlab)
* [coop play](#coop-play)
* [misc other](#misc-other)

## discord

you're probably already in it; in case you aren't, join here: https://discord.gg/nFwGb4SzYF

## install synchrony

* install the latest dev build from https://necro.marukyu.de/
* run Synchrony.exe

## general modding tips

* #mod-help in discord is a helpful, friendly channel. come ask your questions!
* development is ongoing! if something isn't working right it's very possible that
downloading a new version of synchrony will add the features you were trying to use
* when asking for help, please also tell us your synchrony version! e.g. v0.19.2-dev-210303-0500
* see the [how do I log stuff to the console?](#how-do-i-log-stuff-to-the-console) section below for instructions
on how to get printf-debugging working
* all lua code is hot-reloaded! you don't need to restart the game, or
even reload your mod to see changes!

## run an example mod

* go to the "mods" menu ingame
* choose "open mod directory"
* download https://gitlab.com/pancelor/better-slimes and unzip it into your mod folder
* play a game as bard
* find a slime on 1-1; notice the fireballs :)

## synchrony architecture overview

synchrony is an ECS architecture; enemies are entities with a bunch of
components, items are entities with a bunch of different components, etc.

the engine is event-based; if you want to do something every time a level loads,
add an event handler to the `levelLoad` event:
```lua
local event = require "necro.event.Event"
local currentLevel = require "necro.game.level.CurrentLevel"

event.levelLoad.add("doMyCoolThing", {order="entities"}, function(ev)
  local zone,floor=currentLevel.getZone(),currentLevel.getFloor()
  dbg("finished loading zone", zone, "floor", floor)
end)
```

* `event.levelLoad` -- how did I choose this? a combination of two things:
asking experts in #mod-help, and searching
https://marukyu.github.io/synchrony-api/manual/01-events.md.html
for events that looked relevant to what I wanted to do
* `"doMyCoolThing"` -- this can be any unique name you like
* `{order="entities"}` -- this is a "tiebreaker" value. many event handlers are
attached to `event.levelLoad`, and we want to control what order they happen in.
check the list of order keys below any event on
https://marukyu.github.io/synchrony-api/manual/01-events.md.html
to see what your options are. (the different order keys happen
in the order they're listed in)
* if you need further tiebreakers between events attached to the same order key,
use `{order="entities", sequence=1}`. event handlers with negative sequence
numbers happen first, engine/vanilla-necrodancer stuff happens during sequence
number 0, and event handlers with positive sequence numbers happen later.

## components

"components" can be attached to any entities (at load time), and entities can have any components.

an example entity: a red dragon. an example component: position (x/y)

generally in synchrony, you iterate over things by iterating over anything with
a specific set of components, rather than iterating over a specific list of items/enemies/etc.

this is cool and extensible because then other mod authors can add components
to their stuff and your system will automatically work with their stuff that
you didn't even know about when you wrote your mod.

```lua
local ecs = require "system.game.Entities"

event.turn.add("doFireballTurret", {order="aiActions"}, function(ev)
  for turret in ecs.entitiesWithComponents{"betterslimes_fireballTurret"} do
    --do stuff
  end
end
```
(full example: https://gitlab.com/pancelor/better-slimes/-/blob/main/scripts/BetterSlimes.lua)

this is so common that many event handlers (mainly ones that start with
`object`, `holder`, or `inventory`) can be filtered to only apply to entities
with a particular component. for example, these two snippets do the same thing:

```lua
event.objectSpawn.add("arbitraryName1", {order="test"}, function (ev)
  if ev.entity:hasComponent("sprite") then
    ev.entity.sprite.x = 123
  end
end)
```

```lua
event.objectSpawn.add("arbitraryName2", {order="test", filter="sprite"}, function (ev)
  ev.entity.sprite.x = 123
end)
```
(the second snippet is preferred, because the engine can cache the filter)

for another example of adding a new component, see my symmetrognome mod:
https://gitlab.com/pancelor/symmetrognome/-/blob/main/scripts/Symmetrognome.lua
there are 3 basic steps:
1. `components.register` creates a new component
2. `event.entitySchemaLoadNamedEnemy` attaches the new component to all metrognomes
3. `event.objectTakeDamage` fires whenever any entity with that component takes damage

## lua gotchas

a non-comprehensive list:
* arrays are implemented as objects with numbers as keys
* arrays are 1-indexed
* the "+=" operator does not exist (and others like it)
* you can omit function call parentheses sometimes (for 1-argument functions).
so, `require"foo"` is the same as `require("foo")`

----------

## mod-making: required tools

required:
* any text editor

recommended:
* something like the unix `tail` command is _very_ useful;
see the [how do I log stuff to the console?](#how-do-i-log-stuff-to-the-console) section below

## example mods

good places to find example mods:

* the in-game mod portal! (idk how hard it is to see their source code tho)
* https://gitlab.com/groups/Synchrony-Mods/-/shared
  * https://gitlab.com/pancelor/triangle-slime in particular was made to be an
  introductory example mod. note that you will need to also activate
  https://gitlab.com/pancelor/pancelor-enemy-pools for the mod to work
  * note that some of the mods on this page (in particular: ExtendedAPI, ExtendedAPI - Pools
  and CustomOrderAPI) were made for older builds of Synchrony and are broken now.
* #mod-showcase in discord

## making a basic mod

use the "mods" -> "create new mod" option in synchrony, and open up
the folder it generates. add a `scripts` folder and make a new lua
file in that folder (any filename is fine)

you'll probably start modding by extending an existing entity, and giving it
new/different components. try making a new slime! it's pretty easy:

```lua
local action = require "necro.game.system.Action"
local customEntities = require "necro.game.data.CustomEntities"

customEntities.extend {
  name="slime",
  template=customEntities.template.enemy("slime",3), --Orange Slime
  components={
    friendlyName={name="Pentagon Slime"},
    aiPattern={
      moves={
        action.Direction.DOWN_RIGHT,
        action.Direction.DOWN_LEFT,
        action.Direction.UP_LEFT,
        action.Direction.UP,
        action.Direction.RIGHT,
      },
    },
    sprite={texture="mods/pentaslime/images/penta-slime.png"},
  },
}
```
(full mod source code at https://gitlab.com/pancelor/penta-slime)

## file paths

when adding textures and stuff, the file path is a bit odd. e.g.
`sprite={texture="mods/pentaslime/images/penta-slime.png"}` (from above)

* `mods` is a fake/symlink/idk thing that gets you started
* `pentaslime` is the name of your mod from mod.json, **not** the literal
folder name of you mod on your filesystem
* the rest is a normal path within your mod folder

If you use `ext` instead of `mods` you get linked into the vanilla
necrodancer `data` directory; e.g.
`bestiary={image="ext/bestiary/bestiary_ogre.png"}`

## how do I add my custom enemy to the game?

use my enemyPool helper mod: https://gitlab.com/pancelor/pancelor-enemy-pools

for example:

```lua
local enemyPool = require "enemypool.EnemyPool"

event.levelLoad.add("registerPentaSlime", {order="entities"}, function(ev)
  enemyPool.registerConversion{
    from="Slime3",
    to="pentaslime_slime",
    probability=0.3,
  }
end)
```
(full mod at https://gitlab.com/pancelor/penta-slime)

## how do I log stuff to the console?

1. `cd` to the folder containing Synchrony.exe
2. `tail -f Synchrony.log`
3. inside a lua mod, do `dbg("hello", myVariable)`

## how do I spawn in my custom stuff, for testing?

spawntool is a mod that lets you spawn stuff with right-click:
https://cdn.discordapp.com/attachments/771789293382402098/821874141342597190/SpawnTool-1.7.necromod

I recommend not using spawntool for dev work (it's too many steps to re-spawn your enemy/whatever every time you make a change), and instead doing something like this in your mod code:

```lua
local dev
dev=true

...

if dev then
  event.levelLoad.add("spawn", {order="entities"}, function (ev)
    object.spawn("mymod_mycoolenemy",-1,-1)
  end)
end
```

this will spawn your custom enemy in the start room of every floor.

the `dev=true` line can be toggled off with your toggle-comment
hotkey in your text editor, which means the rest of your program
will see `dev` as `nil`, allowing it to skip the dev-only parts of
your code without crashing.

this is nice because you can easily have
all sorts of dev-only functionality that you can be sure will be turned off
whenever you make a new release of your mod, just by toggling off
that one line at the top of your mod

example: https://gitlab.com/pancelor/scatter-mage/-/blob/main/scripts/ScatterMage.lua

## how do I refer to existing stuff?

1. open necrodancer.xml (part of the original game)
2. find your enemy or whatever:
```xml
<dragon type="2" id="403" friendlyName="Red Dragon">
```
3. you can refer to this as "Dragon2" in lua code. (take the xml tag name, and
append the type. however, if `type` is 1, don't append it. so, "Dragon" refers
to the Green Dragon)
4. HOWEVER, when using `customEntities.extend`, you refer to this as
`"dragon",2` instead, like so:
```lua
customEntities.extend {
  name = "dragon",
  template = customEntities.template.enemy("dragon",2),
  ...
```

This also applies to items; e.g. `weapon_spear` in the xml becomes `WeaponSpear` in Synchrony.

## namespacing / how do I refer to modded stuff?

mods are namespaced; if you do `customEntities.extend { name="dragon" }` and
your mod.json file has `"name":"purpledragon"`, then the name you refer to your
new entity by is `"purpledragon_dragon"`.

custom components are also namespaced.

for an example of both, see https://gitlab.com/pancelor/purple-dragon/-/blob/main/scripts/PurpleDragon.lua

## what components exist?

to see what components exist on a given entity (including your own custom entities),
`dbg` them at load time:
```lua
event.entitySchemaLoadNamedEntity.add("debug", {key="Dragon2"}, function (ev)
  dbg(ev.entity)
end)
```

## changing components on an entity

once an entity has been loaded, you can't change the components attached to it.
attach any new components either during creation (e.g. with
`customEntities.extend`) or when the type is loaded, with
`entitySchemaLoadNamedEntity` or `entitySchemaLoadNamedEnemy`:

```lua
-- modify blue slimes only
event.entitySchemaLoadNamedEntity.add("makeSlimesGood", {key="Slime2"}, function (ev)
  ev.entity.betterslimes_fireballTurret = {}
end)

-- modify all slimes and every custom enemy extended from slimes
-- note: this is a different event!! ...Entity v. ...Enemy!
event.entitySchemaLoadNamedEnemy.add("makeSlimesGood", {key="slime"}, function (ev)
  ev.entity.betterslimes_fireballTurret = {}
end)
```
(full example: https://gitlab.com/pancelor/better-slimes/-/blob/main/scripts/BetterSlimes.lua)

## how do I add a custom spell?

that takes a bit more work than just adding a custom enemy. basically, the
spell entity is separate from the spellcast entity. check out
https://gitlab.com/pancelor/scatter-mage/-/blob/main/scripts/ScatterMage.lua
for an example of adding a new spell (note that this spell is bound to an
enemy weapon, not to a item you can find in a purple chest)

## different event kinds

https://discord.com/channels/600372254836129850/600373678231912564/816820762005602355

there are different event kinds:
* enum events
* ordered events
* entity events
* object events

the params are different depending on the event type:
* for enum events the second param is an enum value
* for order events the second param is an order key
* for entity events and object events, the second param is a table of the form
`{order="orderKey", filter={"required components"}}`

how can you tell what kind of event an event is?
* if it starts with object or holder it’s an object event
* if the list of order keys (on the doc site) is empty, it's an enum event

## mod dependencies

synchrony doesn't have mod dependencies (yet), so you'll need to
tell people when they download your mod what the pre-requisite mods
are. otherwise, the game will show an error when you try to activate
your mod.

you can fail more gracefully if you replace the
`local otherModule = require("OtherMod.Module")` with something like this:

```lua
local hasOtherModule, otherModule = pcall(require, "OtherMod.Module")
-- now, do something based on hasOtherModule
```

## custom weapons

modify a weapon's attack pattern by editing `entity.weaponPattern`;
see https://gitlab.com/Qoce/axe-nerf/

for extending an existing weapon, do
```lua
customEntities.extend {
  name = "myCoolWeapon",
  template = customEntities.template.item("weapon_dagger"),
  ...
```
(see https://gitlab.com/Qoce/ender-dagger)

there's also components like `weaponReloadable` and `weaponThrowable` which hold attack patterns for fired guns/crossbows or thrown daggers/spears

for things like staffs, you can add a `repeatTiles = X` property to the pattern, which should repeat the last specified tile

## custom characters

two tips:
1. give your character this component:
```lua
playableCharacter={
  lobbyOrder=1,
},
```
this places them after the base-game characters in the lobby menu
2. give your character a friendlyName component:
```lua
friendlyName={name="pancelor"},
```

example: https://gitlab.com/pancelor/ringmaster
(this also has an example of cursed inventory slots)

## mod settings

often you want to add configurable settings to your mod; you can do this with:

```lua
local settings = require "necro.config.Settings"

healthMultiplier = settings.entitySchema.number {
  name="Health multiplier",
  default=1,
  format=settings.Format.MULTIPLIER,
  step=0.25,
}

event.entitySchemaLoadEnemy.add("multiplyHealth", {order="namedOverrides", sequence=1}, function (ev)
  if ev.entity.health then
    ev.entity.health.health = math.floor(ev.entity.health.health * healthMultiplier)
    ev.entity.health.maxHealth = math.floor(ev.entity.health.maxHealth * healthMultiplier)
  end
end)
```

you can do `settings.shared` and `settings.entitySchema` for server-synced game settings, so those can be gameplay-relevant

or `settings.user` / `settings.overridable` for local settings (which can also optionally be overridden)

more info: https://discord.com/channels/600372254836129850/600373678231912564/824764331044110398

----------

## publishing

make .necromod files in the mods menu, in the "create package" option in the gear next to any mod's name.

make sure to change your mod.json metadata to a larger version number when making a new release

drop your published mods in #mod-showcase on discord!

## gitlab

"how do I get my mod posted on https://gitlab.com/groups/Synchrony-Mods/-/shared ?"

tl;dr:
* make a gitlab account
* upload your code
* ask in #mod-help to be added to the synchrony gitlab group
* go to your project page, "members" > "invite group", and add the synchrony group (access level "reporter", no expiration date)

## coop play

to play online with mods, launch steam in the background, then relaunch synchrony and choose "host online game" in the synchrony menu. then choose "invite friends".

you'll both need to be running the same exact mods, and only .necromod files are allowed (no unpackaged mods). (see [publishing](#publishing))

you'll need to change your local version of the mod to the .necromod file, in the gear menu next to your mod.

send your friends your mods, have them save them in their mods folder, and synchrony will let you know if they're missing any

## misc other

to hoist into its own section: rng, extensible enums:

* https://discord.com/channels/600372254836129850/600373678231912564/825116254036951050
extensible enums / enum data
* https://discord.com/channels/600372254836129850/600373678231912564/825116254036951050
attaching arbitrary data to enums
* https://discord.com/channels/600372254836129850/600373678231912564/816565002991304754
rng channels/seeding NOTE: we have extensible enums now! so this is slightly out of date

other:

* https://discord.com/channels/600372254836129850/600373678231912564/816822985251881002
meme
* https://discord.com/channels/600372254836129850/600373678231912564/815069292738510858
* how to check for physical damage (not bomb/fireball) from a player -- see purpledragon mod
* searching for events on the docs website: open javascript console and do:
`term=/spawn/i; Array.from(document.getElementsByTagName("h2")).map(h2=>h2.innerText).filter(title=>term.exec(title))`
* https://discord.com/channels/600372254836129850/600373678231912564/816553216921436180
"how exactly do the order and sequence values work"
* https://discord.com/channels/600372254836129850/600373678231912564/816560494948122625
"You might not want to use object.convert()"
* https://discord.com/channels/600372254836129850/600373678231912564/816569197027000351
```lua
local entity = ecs.getEntitiesByType("Slime")
while entity:next() do
  dbg(entity.position)
end
```
* https://discord.com/channels/600372254836129850/600373678231912564/816813518078607380
long discussion about what exactly components are, when entities can be modified, etc.
"unlike most ECS architectures, there's no addition/removal of components for entities at runtime"
(discussion ends around here: https://discord.com/channels/600372254836129850/600373678231912564/816819855335751732)
* https://discord.com/channels/600372254836129850/600373678231912564/816821994208624682
"also, for ordered/object/entity, you can shorten {order="test"} to "test";
that's why you sometimes just see strings" (but please don't shorten it! its confusing IMO)
* https://discord.com/channels/600372254836129850/600373678231912564/821556581182472242
"what's the difference between event.levelLoad and event.gamestatelevel"
* https://discord.com/channels/600372254836129850/600373678231912564/821800426537222185
multiplayer-compatible variables / rollback / level-persistent vars / run-persistent vars
* https://discord.com/channels/600372254836129850/600373678231912564/823293039589851216
"how do i go about making custom tiles or modifying existing ones"
* https://discord.com/channels/600372254836129850/600373678231912564/823694012434612234
using `customEntities.register` (instead of `.extend`)
* https://discord.com/channels/600372254836129850/600373678231912564/823952020674314280
"how does remappedMovement work"
* https://discord.com/channels/600372254836129850/600373678231912564/824047002857898015
"can i spawn a chest with an item from the item pool?"
* https://discord.com/channels/600372254836129850/600373678231912564/824278600459091989
"how does creating a shop/pricetags work"
* `gameObject.active`: need to check this when writing custom enemies
(otherwise they'll move in darkness)
* https://discord.com/channels/600372254836129850/600373678231912564/826755920138600468
main useful `require`s you might want at the top of your file
* https://discord.com/channels/600372254836129850/600373678231912564/831192698705936434
"How do I add dependencies to the mod.json?"
* https://discord.com/channels/600372254836129850/600373678231912564/831330780570452027
"Is there an example of adding stuff to the HUD / UI?"
* https://discord.com/channels/600372254836129850/600373678231912564/827270071420190760
checking if an entity is floating
